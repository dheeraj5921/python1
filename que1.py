def isSubsetSum(set, n, sum):
 
    if (sum == 0):
        return True
    if (n == 0):
        return False
 
    # If last element is greater than
    # sum, then ignore it
    if (set[n - 1] > sum):
        return isSubsetSum(set, n - 1, sum)
 
    # else, check if sum can be obtained
    # by any of the following
    # a) including the last element
    # b) excluding the last element
    return isSubsetSum(
        set, n-1, sum) or isSubsetSum(
        set, n-1, sum-set[n-1])
 
 
# main
#making set and taking input in it
set = []
n = len(set)
n = int(input("enter number of element: "))
for i in range(0, n):
    ele = int(input())
    
    set.append(ele)
print(set)
#sum to be found
sum = int(input("enter sum:"))
if (isSubsetSum(set, n, sum) == True):
    print("Found a subset with given sum")
else:
    print("No subset with given sum")
 
 
 
