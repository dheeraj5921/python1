# python1

1. Given a set of non-negative integers, and a value sum, determine if there is a subset of the given set with sum equal to given sum. 

Example: 

Input: set[] = {3, 34, 4, 12, 5, 2}, sum = 9
Output: True  
There is a subset (4, 5) with sum 9.

Input: set[] = {3, 34, 4, 12, 5, 2}, sum = 30
Output: False
There is no subset that add up to 30.

SOLUTION: i made a array set of n no. and then checked if the last element in the array is greater than the sum or not, else the checked the subset with and without last element than made a driver program to run the code where user is asked to put array elements and the sum they want and it will print wether the subset is present or not
===========================================================================================================================================
2.Given a value N, if we want to make change for N cents, and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? The order of coins doesn’t matter.
For example:
 for N = 4 and S = {1,2,3}, there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}.
      So output should be 4. 
 For N = 10 and S = {2, 5, 3, 6}, there are five solutions: {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} and {5,5}.
      So the output should be 5.

SOLUTION: made a table[i] with n+1 rows in bottom up manner ,
initialize all table values as 0
picked all the coins one by one and updates the values of table
after the index is greater than or equal to the value of the picked
coin
took S as the count of solutions 
then i made driver code for taking array input and process that 
according to the value of cents .**
****
===========================================================================================================================================
3.  Write a function that takes an integer value n as input and prints first n lines as shown below.


 	 	 	 	 	 	 	 	 	1	 	 	 	 	 	 	 	 	 
 	 	 	 	 	 	 	 	1	 	1	 	 	 	 	 	 	 	 
 	 	 	 	 	 	 	1	 	2	 	1	 	 	 	 	 	 	 
 	 	 	 	 	 	1	 	3	 	3	 	1	 	 	 	 	 	 
 	 	 	 	 	1	 	4	 	6	 	4	 	1	 	 	 	 	 
 	 	 	 	1	 	5	 	10	 	10	 	5	 	1	 	 	 	 
 	 	 	1	 	6	 	15	 	20	 	15	 	6	 	1	 	 	 
 	 	1	 	7	 	21	 	35	 	35	 	21	 	7	 	1	 	 
 	1	 	8	 	28	 	56	 	70	 	56	 	28	 	8	 	1	 
1	 	9	 	36	 	84	 	126	 	126	 	84	 	36	 	9	 	1

SOLUTION : this is a problem of pascal traingle 
in the code i create a list  and then a inner list then put the condition 
that if the element is first and last in the row it
will be 1.
NOTE:in pascal triangle every number which comes below two no. will be the
addition of them.

======================================================================================================================================================
4.  find the length of the longest subsequence of a given sequence such that all elements of the subsequence are sorted in increasing order. For example, the length of LIS for {10, 22, 9, 33, 21, 50, 41, 60, 80} is 6 and LIS is {10, 22, 33, 50, 60, 80}.

Input: arr[] = {3, 10, 2, 1, 20}
Output: Length of LIS = 3
The longest increasing subsequence is 3, 10, 20

Input: arr[] = {3, 2}
Output: Length of LIS = 1
The longest increasing subsequences are {3} and {2}

Input: arr[] = {50, 3, 10, 7, 40, 80}
Output: Length of LIS = 4
The longest increasing subsequence is {3, 7, 40, 80}

SOLUTION : i made a table[i] for sorting the possible solution for value i , it it constructed in bottom up manner
pick all the coins from the table one by one till the index is greater than equal to the picked coin and then update the values of table
========================================================================================================================

5. Given string x print permutations of a given string.
for example:

x = 'ABC'
Below are the permutations of string ABC. 
ABC ACB BAC BCA CBA CAB

SOLUTION : In this problem i made a function permutation taking string as argument
and took string as input and printed its permutations
================================================================================================================================================

